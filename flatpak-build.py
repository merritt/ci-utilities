#!/usr/bin/env python3
import os, sys, subprocess, json

import yaml


if __name__ == '__main__':
    try:
        modulename = sys.argv[1]
        config = sys.argv[2:]
    except ValueError:
        print("usage: {} module_name [config-opt ...]".format(sys.argv[0]))
        sys.exit(1)

    manifestfile = ".flatpak-manifest"
    # add right extension
    if os.path.exists(f"{manifestfile}.yml"):
        manifestfile = f"{manifestfile}.yml"
    elif os.path.exists(f"{manifestfile}.yaml"):
        manifestfile = f"{manifestfile}.yaml"
    else:
        manifestfile = f"{manifestfile}.json"

    # might be useful for debugging
    subprocess.call(["flatpak", "info", "org.kde.Sdk//5.15-22.08"])
    subprocess.call(["flatpak", "info", "org.kde.Platform//5.15-22.08"])
    subprocess.call(["flatpak", "info", "org.kde.Sdk//5.15-23.08"])
    subprocess.call(["flatpak", "info", "org.kde.Platform//5.15-23.08"])
    subprocess.call(["flatpak", "info", "org.kde.Sdk//6.6"])
    subprocess.call(["flatpak", "info", "org.kde.Platform//6.6"])
    subprocess.call(["flatpak", "info", "org.kde.Sdk//6.7"])
    subprocess.call(["flatpak", "info", "org.kde.Platform//6.7"])
    subprocess.call(["flatpak", "info", "org.kde.Sdk//6.8"])
    subprocess.call(["flatpak", "info", "org.kde.Platform//6.8"])

    # finally, build and install
    subprocess.check_call(["flatpak-builder", "--repo=repo", "--force-clean", "build-dir", "--disable-rofiles-fuse", "--user", manifestfile])

    # Export the result to a bundle
    f = open(manifestfile, "r")
    if manifestfile.endswith(".json"):
        manifest = json.load(f)
    else:
        manifest = yaml.safe_load(f)

    try:
        app_id = manifest["app-id"]
    except KeyError:
        app_id = manifest["id"]

    subprocess.check_call(["flatpak", "build-bundle", "repo", f"{modulename}.flatpak", app_id, "master"])

    # Export the translation and debug information as well
    # Due to appstream-isms we have to ensure the IDs are sanitized to match what Flatpak does
    locale_id = app_id.replace('-', '_') + ".Locale"
    try:
        subprocess.check_call(["flatpak", "build-bundle", "--runtime", "repo", f"{modulename}-locale.flatpak", locale_id, "master"])
    except subprocess.CalledProcessError as e:
        # log the failure, but treat it as non-fatal
        print(f"Warning: Building bundle for {locale_id} failed with exit code {e.returncode}", file=sys.stderr)
    debug_id = app_id.replace('-', '_') + ".Debug"
    try:
        subprocess.check_call(["flatpak", "build-bundle", "--runtime", "repo", f"{modulename}-debug.flatpak", debug_id, "master"])
    except subprocess.CalledProcessError as e:
        # log the failure, but treat it as non-fatal
        print(f"Warning: Building bundle for {debug_id} failed with exit code {e.returncode}", file=sys.stderr)
